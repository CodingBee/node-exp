const express = require("express");
const path = require('path');
const app = express();
const fs = require('fs');
const players = require('./data');
const playerItem = require('./render');
const encrypt = require('./crypto');
const logger = require('./logger');

app.listen(3000, function () {
    console.log('Eric\'s server started on http://localhost:3000');
    logger('server started');
});

// static files
// using api "static" to specify the directory 
app.use('/nba-players', express.static(path.join(__dirname, '/assets/nba_players')));

// Response when users connect to the root dir.
app.all("/", function (req, res) {
    // NBA Players
    const items = Object.keys(players).map(name => playerItem(players[name]));

    fs.readFile('./intro.txt', 'utf8', (err, data) => {
        if (err) {
            return;
        }

        res.send(
            "<h1>NBA Fantasy</h1>"
            + '\"je t\'aime\" is encrypted as ' + '<b>' + encrypt("je t'aime") + '</b><br />'
            + data + '<br />'
            + "<h2>Favorite Guards: Ordered by the first name</h2>"
            + "<ul>" + items.join("") + "</ul>"
        );
    })
    // next()
    // if the 3rd param "next" is specified,
    // the route control is passed to next route definition
});

// Create HTTP error

function createError(status, message) {
    var err = new Error(message);
    err.status = status;
    return err;
}

// verify parameters of request
app.param('players', function (req, res, next, name) {
    if (req.players = players[name.toLowerCase()]) {
        next();
    } else {
        next(createError(404, 'failed to find model'));
    }
});

// Error handling
app.use(function (req, res, next) {
    res.status(404).send('Sorry cant find that!');
});
