module.exports = {
    "conley": {
        fullName: "Mike Conley",
        photo: 201144,
        fantasyInfo: 4246
    },
    "curry": {
        fullName: "Stephen Curry",
        photo: 201939,
        fantasyInfo: 4612
    },
    "kyrie": {
        fullName: "Kyrie Irving",
        photo: 202681,
        fantasyInfo: 4840
    },
    "lillard": {
        fullName: "Damian Lillard",
        photo: 203081,
        fantasyInfo: 5012
    },
    "rose": {
        fullName: "Derrick Rose",
        photo: 201565,
        fantasyInfo: 4387
    }
};
