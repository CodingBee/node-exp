const express = require("express");
const path = require('path');
const app = express(); // generate Express App

app.listen(3000, function () {
    console.log('Eric\'s server started on http://localhost:3000');
});

// static files
// using api "static" to specify the directory 
app.use('/model-album', express.static(path.join(__dirname, '/assets/models')));
app.use('/nba-players', express.static(path.join(__dirname, '/assets/nba_players')));
app.use('/users', express.static(path.join(__dirname, '/assets/users')));

const models = ["candice", "joanna", "lucy", "maki", "huang"];
const players = {
    "conley": {
        fullName: "Mike Conley",
        photo: 201144,
        fantasyInfo: 4246
    },
    "curry": {
        fullName: "Stephen Curry",
        photo: 201939,
        fantasyInfo: 4612
    },
    "kyrie": {
        fullName: "Kyrie Irving",
        photo: 202681,
        fantasyInfo: 4840
    },
    "lillard": {
        fullName: "Damian Lillard",
        photo: 203081,
        fantasyInfo: 5012
    },
    "rose": {
        fullName: "Derrick Rose",
        photo: 201565,
        fantasyInfo: 4387
    },
};

const playersFantasyInfo = name =>
    `https://sports.yahoo.com/nba/players/${players[name].fantasyInfo}`;

const playerPhotoDom = name => {
    const HOST = '//ak-static.cms.nba.com/wp-content/uploads/headshots/nba/latest/260x190/';
    const url = HOST + players[name].photo + '.png';
    return `<img class="player-img" src="${url}"></img>`;
}

const playerItem = name => {
    const url = playersFantasyInfo(name);
    const photo = playerPhotoDom(name);
    const fullName = players[name].fullName;
    return `<li><h3><a href="${url}" target="_blank">${photo}<br />${fullName}</a></h3></li>`;
}

// Response when users connect to the root dir.
// all: all requests will trigger this at first
app.all("/", function (req, res) {
    // Object.keys(req).map(key => {
    //     console.log(key);
    // });

    // NBA Players
    const items = Object.keys(players).map(playerItem);

    res.send(
        "<h1>NBA Fantasy</h1>"
        + "<h2>Top 5 Guard</h2>"
        + "<ul>" + items.join("") + "</ul>"
    );

    // Model
    // const modelItems = models.map(name => "<li>" + name + "</li>");
    // res.send(
    //     "<b>Beauty</b> Model Album!<br />Visit /models/ModelName <br />"
    //     + "<h2>Model List:</h2>"
    //     + "<ul>" + modelItems.join("") + "</ul>"
    // );

    // next()
    // if the 3rd param "next" is specified,
    // the route control is passed to next route definition
});

// Create HTTP error

function createError(status, message) {
    var err = new Error(message);
    err.status = status;
    return err;
}

// Load model by name
// verify parameters of request
app.param('models', function (req, res, next, name) {
    if (req.models = models[name.toLowerCase()]) {
        next();
    } else {
        next(createError(404, 'failed to find model'));
    }
});

app.param('players', function (req, res, next, name) {
    if (req.players = players[name.toLowerCase()]) {
        next();
    } else {
        next(createError(404, 'failed to find model'));
    }
});

/**
 * GET :model.
 */

app.get('/models/:name', function (req, res, next) {
    const filePath = path.join(__dirname, '/assets/models', req.params.name + '.jpg');
    //res.send('Model ' + req.params.name + '<b>' + filePath + '</b>');
    res.download(filePath, function (err) {
        if (!err) return; // file sent
        if (err && err.status !== 404) return next(err); // non-404 error
        // file for download not found
        res.statusCode = 404;
        res.send('Cant find that file, sorry!');
    });
});



// Routing
var setGetRoute = function (path, result) {
    app.get(path, function (req, res) {
        res.send(result);
    });
};

setGetRoute('/mypath', 'this is mypath');

// Error handling
app.use(function (req, res, next) {
    res.status(404).send('Sorry cant find that!');
});
