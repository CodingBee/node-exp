const fs = require('fs');
const { Console } = require('console');

const output = fs.createWriteStream('./stdout.log');
const errorOutput = fs.createWriteStream('./stderr.log');
// custom simple logger
const logger = new Console({ stdout: output, stderr: errorOutput });
var count = 0;

module.exports = function (msg) {
    logger.log(`${count}: ${msg} <${new Date()}>\r\n`);
    count++;
};
