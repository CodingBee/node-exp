
const playersFantasyInfo = player =>
    `https://sports.yahoo.com/nba/players/${player.fantasyInfo}`;

const playerPhotoDom = player => {
    const HOST = '//ak-static.cms.nba.com/wp-content/uploads/headshots/nba/latest/260x190/';
    const url = HOST + player.photo + '.png';
    return `<img class="player-img" src="${url}"></img>`;
}

module.exports = function (player) {
    const url = playersFantasyInfo(player);
    const photo = playerPhotoDom(player);
    const fullName = player.fullName;

    return `<li><h3><a href="${url}" target="_blank">${photo}<br />${fullName}</a></h3></li>`;
};

// module.exports = {
//     playerItem: function (player) {
//         const url = playersFantasyInfo(player);
//         const photo = playerPhotoDom(player);
//         const fullName = player.fullName;
//         return `<li><h3><a href="${url}" target="_blank">${photo}<br />${fullName}</a></h3></li>`;
//     }
// };
