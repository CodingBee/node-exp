const crypto = require('crypto');
const logger = require('./logger');

module.exports = function (plainText) {
    const encrypted = crypto.createHash('md5').update(plainText).digest("hex");
    logger(`${plainText} is encoded as ${encrypted}`)

    return encrypted;
};
